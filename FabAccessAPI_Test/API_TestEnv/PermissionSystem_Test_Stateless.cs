﻿using FabAccessAPI;
using FabAccessAPI.Schema;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FabAccessAPI_Test.API_TestEnv
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [Order(1)]
    public class PermissionSystem_Test_Stateless
    {
        [TestCase("Admin1", true)]
        [TestCase("ManagerA1", true)]
        [TestCase("MakerA1", true)]
        [TestCase("GuestA1", true)]
        [Order(1)]
        public async Task AccessPermissionSystem(string username, bool expectInterface)
        {
            API api = new API();
            ConnectionData connectionData = TestEnv.CreateConnetionData(username);
            await api.Connect(connectionData);

            bool result = api.Session.PermissionSystem != null;

            await api.Disconnect();

            Assert.AreEqual(expectInterface, result);
        }

        [TestCase("Admin1", true)]
        [TestCase("ManagerA1", true)]
        [TestCase("MakerA1", true)]
        [TestCase("GuestA1", true)]
        [Order(2)]
        public async Task InfoInterface(string username, bool expectInterface)
        {
            API api = new API();
            ConnectionData connectionData = TestEnv.CreateConnetionData(username);
            await api.Connect(connectionData);

            PermissionSystem.InfoInterface_Proxy infoInterface = (PermissionSystem.InfoInterface_Proxy)api.Session.PermissionSystem.Info;

            bool result = !infoInterface.IsNull;

            await api.Disconnect();

            Assert.AreEqual(expectInterface, result);
        }

        [TestCase("Admin1", 13)]
        [TestCase("ManagerA1", 13)]
        [TestCase("MakerA1", 13)]
        [TestCase("GuestA1", 13)]
        [Order(3), Ignore("Not implemented")]
        public async Task ListRoles(string username, int expectRolesCount)
        {
            API api = new API();
            ConnectionData connectionData = TestEnv.CreateConnetionData(username);
            await api.Connect(connectionData);

            IReadOnlyList<Role> roles_list = await api.Session.PermissionSystem.Info.GetRoleList().ConfigureAwait(false);

            await api.Disconnect();

            Assert.AreEqual(expectRolesCount, roles_list.Count);
        }
    }
}

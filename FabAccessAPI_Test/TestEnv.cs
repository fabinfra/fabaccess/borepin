﻿using FabAccessAPI;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace FabAccessAPI_Test
{
    public static class TestEnv
    {
        public const string SCHEMA = "fabaccess";
        public const string TESTSERVER = "127.0.0.1";//"test.fab-access.org";
        public const int TESTSERVER_PORT = 59661;
        public const string PASSWORD = "secret";

        [TestCase("Testuser")]
        public static ConnectionData CreateConnetionData(string username)
        {
            ConnectionData connectionData = new ConnectionData()
            {
                Host = new UriBuilder(TestEnv.SCHEMA, TestEnv.TESTSERVER, TestEnv.TESTSERVER_PORT).Uri,
                Mechanism = SASLMechanismEnum.PLAIN,
                Username = username,
                Properties = new Dictionary<string, object>()
                {
                    { "Username", username },
                    { "Password", TestEnv.PASSWORD },
                },
            };

            return connectionData;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace FabAccessAPI
{
    public class ServerData
    {
        public Schema.Version APIVersion;
        public string ServerName;
        public string ServerRelease;
        public List<string> Mechanisms;
    }
}

﻿namespace FabAccessAPI
{
    public enum ConnectionStatusChanged
    {
        /// <summary>
        /// API-Service has established connection to server
        /// </summary>
        Connected,

        /// <summary>
        /// API-Service has closed the connection to a server
        /// </summary>
        Disconnected,

        /// <summary>
        /// Connection was lost and the API-Service will try to reconnect automatically
        /// </summary>
        ConnectionLoss
    }
}

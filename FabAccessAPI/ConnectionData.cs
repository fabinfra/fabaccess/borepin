﻿using System;
using System.Collections.Generic;

namespace FabAccessAPI
{
    /// <summary>
    /// Data to establish a connection to a server
    /// With Data for Authentication
    /// </summary>
    public class ConnectionData
    {
        public Uri Host;
        public string Username;
        
        public SASLMechanismEnum Mechanism;
        public Dictionary<string, object> Properties;
        
        public DateTime LastTime;

        public override bool Equals(object? obj)
        {
            if(obj is ConnectionData && obj != null)
            {
                ConnectionData? data = obj as ConnectionData;

                return data.Host.Host == Host.Host &&
                    data.Host.Port == Host.Port &&
                    data.Mechanism == Mechanism &&
                    data.Username == Username;
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hashCode = -1151110446;
            hashCode = hashCode * -1521134295 + EqualityComparer<Uri>.Default.GetHashCode(Host);
            hashCode = hashCode * -1521134295 + Mechanism.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Username);
            hashCode = hashCode * -1521134295 + EqualityComparer<Dictionary<string, object>>.Default.GetHashCode(Properties);
            return hashCode;
        }
    }
}

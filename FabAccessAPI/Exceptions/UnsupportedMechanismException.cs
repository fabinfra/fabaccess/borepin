﻿using System;

namespace FabAccessAPI.Exceptions
{
    public class UnsupportedMechanismException : Exception
    {
        public UnsupportedMechanismException()
        {

        }

        public UnsupportedMechanismException(string message) : base(message)
        {

        }

        public UnsupportedMechanismException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}

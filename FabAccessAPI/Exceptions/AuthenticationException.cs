﻿using System;

namespace FabAccessAPI.Exceptions
{
    /// <summary>
    /// Authenticating to a server has failed
    /// InnerException will provide more information
    /// </summary>
    public class AuthenticationException : Exception
    {
        public AuthenticationException()
        {

        }

        public AuthenticationException(string message) : base(message)
        {

        }

        public AuthenticationException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}

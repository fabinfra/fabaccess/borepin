﻿using System;

namespace FabAccessAPI.Exceptions
{
    public class APIIncompatibleException : Exception
    {
        public APIIncompatibleException()
        {

        }

        public APIIncompatibleException(string message) : base(message)
        {

        }

        public APIIncompatibleException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}

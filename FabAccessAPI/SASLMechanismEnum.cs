﻿using System;

namespace FabAccessAPI
{
    public enum SASLMechanismEnum
    {
        PLAIN,
    }

    public static class SASLMechanism
    {
        public static string ToString(SASLMechanismEnum mechanism)
        {
            switch(mechanism)
            {
                case SASLMechanismEnum.PLAIN:
                    return "PLAIN";
                default:
                    throw new ArgumentException("Mechanism unknown.");
            }
        }
    }

}

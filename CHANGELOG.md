# Revision history for Borepin

## 0.3.11 -- 2023-03-17
Final INTERFACER Release

### Features
* improved API
* create and bind SmartCards to users
* improved UI
* brand and template UI with theme file

### Updates
* create DES-Fire Cards with FabFire-Protocol
* identify machines with NTAG
* unlock machines with electric locks
* search for users
* bind and unbind FabFire-Card to users
* reconnect to server if connection is lost
* autoconnect to server on client start
* improved feedback for users if client lost connection

## 0.3.5 -- 2022-07-12
Beta INTERFACER Release

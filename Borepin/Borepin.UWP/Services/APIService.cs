﻿using Borepin.Service;
using FabAccessAPI;

namespace Borepin.UWP.Services
{
    public class APIService : IAPIService
    {
        #region Private Members
        private readonly IAPI _API;
        #endregion

        #region Constructors
        public APIService()
        {
            _API = new API();
        }
        #endregion
        public IAPI GetAPI()
        {
            return _API;
        }
    }
}

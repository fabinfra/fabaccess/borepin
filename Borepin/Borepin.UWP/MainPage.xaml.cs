﻿namespace Borepin.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new Borepin.App(new PlatformInitializer()));
        }
    }
}

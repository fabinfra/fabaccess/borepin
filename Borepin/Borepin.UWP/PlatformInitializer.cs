﻿using Borepin.UWP.Services;
using Prism;
using Prism.Ioc;
using Borepin.Service.Storage;
using Borepin.Service.Versioning;
using Borepin.Service;
using Borepin.Service.Browser;
using NFC.PCSC;
using NFC.Interfaces;

namespace Borepin.UWP
{
    public class PlatformInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IPreferenceStorageService, PreferenceStorageService>();
            containerRegistry.Register<ISecretStorageService, SecretStorageService>();
            containerRegistry.Register<IVersioningService, VersioningService>();
            containerRegistry.Register<IBrowserService, BrowserService>();
            containerRegistry.Register<INFCService, NFCService>();

            containerRegistry.RegisterSingleton<IAPIService, APIService>();
        }
    }
}

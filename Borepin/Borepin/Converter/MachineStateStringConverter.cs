﻿using FabAccessAPI.Schema;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Borepin.Converter
{
    public class MachineStateStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch((Machine.MachineState)value)
            {
                case Machine.MachineState.free:
                    return "Free";
                case Machine.MachineState.inUse:
                    return "In Use";
                case Machine.MachineState.toCheck:
                    return "To Check";
                case Machine.MachineState.reserved:
                    return "Reserved";
                case Machine.MachineState.blocked:
                    return "Blocked";
                case Machine.MachineState.disabled:
                    return "Disabled";
                case Machine.MachineState.totakeover:
                    return "ToTakeOver";
                default:
                    return "Unknown";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}

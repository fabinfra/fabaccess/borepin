﻿using FabAccessAPI.Schema;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Borepin.Converter
{
    public class MachineStateColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch((Machine.MachineState)value)
            {
                case Machine.MachineState.free:
                    return (Color)Application.Current.Resources["FirstColor"];
                default:
                    return (Color)Application.Current.Resources["SixthColor"];
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}

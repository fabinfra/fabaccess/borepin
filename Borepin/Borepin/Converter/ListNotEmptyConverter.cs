﻿using System;
using System.Collections;
using System.Globalization;
using Xamarin.Forms;

namespace Borepin.Converter
{
    public class ListNotEmptyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && value is IList)
            {
                ICollection collection = value as ICollection;
                return collection.Count > 0;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}

﻿using Prism.Ioc;
using Borepin.PageModel;
using Borepin.Page;
using Xamarin.Forms;
using Borepin.Dialog;
using Borepin.DialogModel;
using Prism;
using Borepin.Page.SetUpProcess;
using Borepin.PageModel.SetUpProcess;
using Borepin.Page.AddServerProcess;
using Borepin.PageModel.AddServerProcess;
using System;
using Borepin.Service.Storage;
using NLog;
using Borepin.Service.ErrorMessage;
using Prism.Navigation.Xaml;
using Prism.Navigation;
using Borepin.Service;
using FabAccessAPI;
using FabAccessAPI.Schema;
using System.Threading.Tasks;
using System.Collections.Generic;
using Prism.Services;

namespace Borepin
{
    public partial class App
    {
        public App(IPlatformInitializer platformInitializer) : base(platformInitializer)
        {
            NLog.Config.LoggingConfiguration config = new NLog.Config.LoggingConfiguration();
            NLog.Targets.ConsoleTarget logconsole = new NLog.Targets.ConsoleTarget("logconsole");
            config.AddRule(LogLevel.Trace, LogLevel.Fatal, logconsole);
            LogManager.Configuration = config;
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync(new Uri("https://borepin.fab-access.org/StartPage")).ConfigureAwait(false);
        }


        protected override async void OnAppLinkRequestReceived(Uri uri)
        {
            IPageDialogService pageDialogService = Container.Resolve<IPageDialogService>();

            if (uri.LocalPath.StartsWith("/resource/", StringComparison.OrdinalIgnoreCase))
            {
                if (Container.IsRegistered<IAPIService>() && Container.IsRegistered<ILoginStorageService>())
                {
                    string resource_id = uri.LocalPath.Remove(0, "/resource/".Length);

                    IAPIService apiService = Container.Resolve<IAPIService>();
                    IAPI api = apiService.GetAPI();

                    if (api.IsConnected)
                    {
                        Optional<Machine> optional = await api.Session.MachineSystem.Info.GetMachine(resource_id).ConfigureAwait(false);

                        if (optional.Just == null)
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                await pageDialogService.DisplayAlertAsync(Borepin.Resources.Text.TextResource.ALERT, Borepin.Resources.Text.TextResource.ALERT_ID, Borepin.Resources.Text.TextResource.OK).ConfigureAwait(false);
                            });
                            return;
                        }
                            
                        Prism.Navigation.NavigationParameters parameters = new Prism.Navigation.NavigationParameters
                        {
                            { "instance", optional.Just.Id },
                        };

                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            INavigationResult result = await Container.Resolve<INavigationService>().NavigateAsync("/MainPage/NavigationPage/MachineListPage/MachinePage", parameters).ConfigureAwait(false);
                        });
                    }
                }
                else
                {
                    return;
                }
            }

        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            #region Register Basic Navigation
            containerRegistry.RegisterForNavigation<MainPage, MainPageModel>();
            containerRegistry.RegisterForNavigation<NavigationPage>();

            containerRegistry.RegisterForNavigation<StartPage, StartPageModel>();

            containerRegistry.RegisterForNavigation<MachinePage, MachinePageModel>();
            containerRegistry.RegisterForNavigation<SettingsPage>();
            containerRegistry.RegisterForNavigation<MachineListPage, MachineListPageModel>();
            containerRegistry.RegisterForNavigation<ServerListPage, ServerListPageModel>();
            containerRegistry.RegisterForNavigation<ServerPage, ServerPageModel>();
            containerRegistry.RegisterForNavigation<ScanPage, ScanPageModel>();
            containerRegistry.RegisterForNavigation<ScanURNPage, ScanURNPageModel>();

            containerRegistry.RegisterForNavigation<UserListPage, UserListPageModel>();
            containerRegistry.RegisterForNavigation<UserPage, UserPageModel>();
            containerRegistry.RegisterForNavigation<CreateCardPage, CreateCardPageModel>();
            containerRegistry.RegisterForNavigation<AddUserPage, AddUserPageModel>();
            containerRegistry.RegisterForNavigation<ProfilePage, ProfilePageModel>();
            #endregion

            #region Register Sequence Navigation
            containerRegistry.RegisterForNavigation<WelcomePage, WelcomePageModel>("SetUpProcess_WelcomePage");

            containerRegistry.RegisterForNavigation<AuthPlainPage, AuthPlainPageModel>("AddServerProcess_AuthPlainPage");
            containerRegistry.RegisterForNavigation<SelectServerPage, SelectServerPageModel>("AddServerProcess_SelectServerPage");
            containerRegistry.RegisterForNavigation<ChooseAuthTypePage, ChooseAuthTypePageModel>("AddServerProcess_ChooseAuthTypePage");
            #endregion

            #region Register Dialog
            containerRegistry.RegisterDialog<ConfirmDialog, ConfirmDialogModel>();
            #endregion

            #region Register Service
            containerRegistry.RegisterSingleton<ILoginStorageService, LoginStorageService>();
            containerRegistry.RegisterSingleton<IErrorMessageService, ErrorMessageService>();

            // NEED PLATFORM SPECIFIC SERVICE
            // IPreferenceStorageService
            // ISecretStorageService
            // IVersioningService
            // IAPIService
            #endregion
        }
    }
}

﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserListItemView : ContentView
    {
        public UserListItemView()
        {
            InitializeComponent();
        }
    }
}
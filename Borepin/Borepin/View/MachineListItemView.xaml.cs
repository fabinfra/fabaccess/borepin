﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MachineListItemView : ContentView
    {
        public MachineListItemView()
        {
            InitializeComponent();
        }
    }
}
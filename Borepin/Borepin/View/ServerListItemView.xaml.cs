﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServerListItemView : ContentView
    {
        public ServerListItemView()
        {
            InitializeComponent();
        }
    }
}
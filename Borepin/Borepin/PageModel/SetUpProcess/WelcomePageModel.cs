﻿using Borepin.Base;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace Borepin.PageModel.SetUpProcess
{
    public class WelcomePageModel : PageModelBase
    {
        #region Constructors
        public WelcomePageModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService, pageDialogService)
        {
            NextCommand = new DelegateCommand<object>(NextCommandCommandExecute);
        }
        #endregion

        #region Commands
        private ICommand _NextCommand;
        public ICommand NextCommand
        {
            get => _NextCommand;
            set => SetProperty(ref _NextCommand, value);
        }
        public void NextCommandCommandExecute(object obj)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                INavigationResult result = await _NavigationService.NavigateAsync("AddServerProcess_SelectServerPage").ConfigureAwait(false);
                if (result.Exception != null)
                {
                    Log.Fatal(result.Exception, "Navigating failed");
                }
            });
            
        }
        #endregion
    }
}

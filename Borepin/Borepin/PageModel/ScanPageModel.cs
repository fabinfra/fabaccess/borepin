﻿using Borepin.Base;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing;
using ZXing.Mobile;

namespace Borepin.PageModel
{
    class ScanPageModel : PageModelBase
    {
        #region Private Fields
        private object _Instance;
        #endregion

        #region Contructors
        public ScanPageModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService, pageDialogService)
        {
            CancelCommand = new DelegateCommand(async () => await CancelCommandExecute().ConfigureAwait(true));
            ScannedCommand = new DelegateCommand(ScannedCommandExecute);

            IsVisible = true;
            IsScanning = true;
        }
        #endregion

        #region Data
        public override Task LoadInstance(object instance)
        {
            _Instance = instance;
            return Task.CompletedTask;
        }

        public override Task<object> CreateInstance()
        {
            return Task.FromResult(_Instance);
        }
        #endregion

        #region Fields
        public MobileBarcodeScanningOptions ScanOptions
        {
            get
            {
                return new MobileBarcodeScanningOptions()
                {
                    PossibleFormats = new List<BarcodeFormat>()
                    {
                        BarcodeFormat.QR_CODE,
                        BarcodeFormat.EAN_13,
                    },
                    AutoRotate = true,
                    DelayBetweenContinuousScans = 300,
                };
            }
        }

        private Result _ScanResult;
        public Result ScanResult
        {
            get => _ScanResult;
            set => SetProperty(ref _ScanResult, value);
        }

        private bool _IsScanning;
        public bool IsScanning
        {
            get => _IsScanning;
            set => SetProperty(ref _IsScanning, value);
        }

        private bool _IsVisible;
        public bool IsVisible
        {
            get => _IsVisible;
            set => SetProperty(ref _IsVisible, value);
        }
        #endregion

        #region Commands
        private ICommand _ScannedCommand;

        public ICommand ScannedCommand
        {
            get => _ScannedCommand;
            set => SetProperty(ref _ScannedCommand, value);
        }
        public void ScannedCommandExecute()
        {
            IsScanning = false;

            INavigationParameters parameters = new NavigationParameters()
            {
                { "result", "scanned" },
                { "value", ScanResult.Text },
            };

            Device.BeginInvokeOnMainThread(async () =>
            {
                INavigationResult result = await _NavigationService.GoBackAsync(parameters).ConfigureAwait(false);
            });
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get => _CancelCommand;
            set => SetProperty(ref _CancelCommand, value);
        }
        public async Task CancelCommandExecute()
        {
            IsScanning = false;

            INavigationParameters parameters = new NavigationParameters()
            {
                { "result", "canceled" },
            };
            await _NavigationService.GoBackAsync(parameters).ConfigureAwait(false);
        }
        #endregion
    }
}

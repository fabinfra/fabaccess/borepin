﻿using Borepin.Base;
using Prism.Commands;
using Prism.Navigation;
using System.Threading.Tasks;
using System.Windows.Input;
using FabAccessAPI.Schema;
using Prism.Services;
using Borepin.Service;
using Capnp.Rpc;
using System;

namespace Borepin.PageModel
{
    public class ProfilePageModel : ConnectionModelBase
    {
        #region Contructors
        public ProfilePageModel(INavigationService navigationService, IPageDialogService pageDialogService, IAPIService apiService) : base(navigationService, pageDialogService, apiService)
        {
            UpdatePasswordCommand = new DelegateCommand(UpdatePasswordCommandExecute);
        }
        #endregion

        #region Data
        public override async Task LoadAPIData()
        {
            User self = await _API.Session.UserSystem.Info.GetUserSelf().ConfigureAwait(false);
            Username = self.Username;

            CanManage = !((User.ManageInterface_Proxy)self.Manage).IsNull;
        }
        #endregion

        #region Fields

        private string _Username;
        public string Username
        {
            get => _Username;
            set => SetProperty(ref _Username, value);
        }

        private string _OldPassword = null;
        public string OldPassword
        {
            get => _OldPassword;
            set => SetProperty(ref _OldPassword, value);
        }

        private string _NewPassword = null;
        public string NewPassword
        {
            get => _NewPassword;
            set => SetProperty(ref _NewPassword, value);
        }

        private bool _CanManage;
        public bool CanManage
        {
            get => _CanManage;
            set => SetProperty(ref _CanManage, value);
        }
        #endregion

        #region Commands
        private ICommand _UpdatePasswordCommand;
        public ICommand UpdatePasswordCommand
        {
            get => _UpdatePasswordCommand;
            set => SetProperty(ref _UpdatePasswordCommand, value);
        }

        public async void UpdatePasswordCommandExecute()
        {
            IsBusy = true;
            if (_API.IsConnected)
            {
                try
                {
                    if(OldPassword != null && OldPassword != string.Empty && NewPassword != null && NewPassword != string.Empty)
                    {
                        User self = await _API.Session.UserSystem.Info.GetUserSelf().ConfigureAwait(false);
                        await self.Manage.Pwd(OldPassword, NewPassword).ConfigureAwait(false);
                        OldPassword = string.Empty;
                        NewPassword = string.Empty;
                    }
                }
                catch (RpcException exception) when (string.Equals(exception.Message, "RPC connection is broken. Task would never return.", StringComparison.Ordinal))
                {
                    Log.Debug("RPC Connection Loss");
                }
            }
            IsBusy = false;
        }
        #endregion
    }
}

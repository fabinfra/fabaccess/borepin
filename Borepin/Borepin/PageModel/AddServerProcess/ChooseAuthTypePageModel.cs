﻿using Borepin.Base;
using Borepin.Base.Exceptions;
using FabAccessAPI;
using Prism.Navigation;
using Prism.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Borepin.PageModel.AddServerProcess
{
    public class ChooseAuthTypePageModel : PageModelBase
    {
        #region Private Fields
        private ConnectionData _ConnectionData;
        #endregion

        #region Contructors
        public ChooseAuthTypePageModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService, pageDialogService)
        {
            AuthPlainCommand = new Command(AuthPlainCommandExecute);
        }
        #endregion

        #region LoadData
        public override Task LoadInstance(object instance)
        {
            if(instance is ConnectionData)
            {
                _ConnectionData = instance as ConnectionData;
            }
            else
            {
                throw new InstanceIncorrectException();
            }
            return Task.CompletedTask;
        }

        public override Task<object> CreateInstance()
        {
            return Task.FromResult<object>(_ConnectionData);
        }
        #endregion

        #region Commands
        private ICommand _AuthPlainCommand;
        public ICommand AuthPlainCommand
        {
            get => _AuthPlainCommand;
            set => SetProperty(ref _AuthPlainCommand, value);
        }
        public async void AuthPlainCommandExecute()
        {
            _ConnectionData.Mechanism = SASLMechanismEnum.PLAIN;

            INavigationResult result = await _NavigationService.NavigateAsync("AddServerProcess_AuthPlainPage").ConfigureAwait(false);
            if(result.Exception != null)
            {
                Log.Fatal(result.Exception, "Navigating failed");
            }
        }
        #endregion
    }
}

﻿using System.Threading.Tasks;
using System.Windows.Input;
using Borepin.Base;
using Borepin.Service;
using Borepin.Service.Versioning;
using FabAccessAPI.Schema;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Borepin.PageModel
{
    public class MainPageModel : ConnectionModelBase
    {
        #region Private Members
        private readonly IVersioningService _VersioningService;
        #endregion

        #region Constructors
        public MainPageModel(INavigationService navigationService, IPageDialogService pageDialogService, IAPIService apiService, IVersioningService versioningService) : base(navigationService, pageDialogService, apiService)
        {
            _VersioningService = versioningService;
            CurrentVersion = _VersioningService.CurrentVersion;
            CurrentBuild = _VersioningService.CurrentBuild;

            NavigateCommand = new Command<string>(NavigateCommandExecute);
        }
        #endregion

        #region LoadData
        public override Task LoadAPIData()
        {
            UserSystem.ManageInterface_Proxy manageInterface = (UserSystem.ManageInterface_Proxy)_API.Session.UserSystem.Manage;
            CanManageUsers = !manageInterface.IsNull;

            return Task.CompletedTask;
        }
        #endregion

        #region Fields
        private string _CurrentVersion;
        public string CurrentVersion
        {
            get => _CurrentVersion;
            set => SetProperty(ref _CurrentVersion, value);
        }

        private string _CurrentBuild;
        public string CurrentBuild
        {
            get => _CurrentBuild;
            set => SetProperty(ref _CurrentBuild, value);
        }

        private bool _CanManageUsers;
        public bool CanManageUsers
        {
            get => _CanManageUsers;
            set => SetProperty(ref _CanManageUsers, value);
        }
        #endregion

        #region Commands
        private ICommand _NavigationCommand;
        public ICommand NavigateCommand 
        {
            get => _NavigationCommand;
            set => SetProperty(ref _NavigationCommand, value);
        }
        public async void NavigateCommandExecute(string view)
        {
            INavigationResult result = await _NavigationService.NavigateAsync($"NavigationPage/{ view }").ConfigureAwait(false);
            if(result.Exception != null)
            {
                Log.Fatal(result.Exception, "Navigating failed");
            }
        }
        #endregion
    }
}

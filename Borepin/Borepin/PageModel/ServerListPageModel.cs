﻿using Borepin.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Navigation;
using Borepin.Base;
using Borepin.Service.Storage;
using FabAccessAPI;
using Borepin.Service;
using Prism.Services;

namespace Borepin.PageModel
{
    public class ServerListPageModel : PageModelBase
    {
        #region Private Fields
        private readonly ILoginStorageService _LoginStorageService;
        private readonly IAPI _API;
        #endregion

        #region Constructors
        public ServerListPageModel(INavigationService navigationService, IPageDialogService pageDialogService, ILoginStorageService loginStorageService, IAPIService apiService) : base(navigationService, pageDialogService)
        {
            _LoginStorageService = loginStorageService;
            _API = apiService.GetAPI();

            AddInstancesCommand = new DelegateCommand(AddInstancesCommandExecute);
            SelectInstanceCommand = new DelegateCommand<object>(SelectInstanceCommandExecute);
        }
        #endregion

        #region LoadData
        public override async Task LoadInstance(object instance)
        {
            List<Task> tasks = new List<Task>();

            IList<ConnectionData> list = await _LoginStorageService.GetList().ConfigureAwait(false);
            if (_API.IsConnected || _API.IsConnecting)
            {
                ActiveConnection = new ServerListItemViewModel(_NavigationService, _PageDialogService);
                await ActiveConnection.LoadInstance(_API.ConnectionData).ConfigureAwait(false);

                list.Remove(_API.ConnectionData);

                HasActiveConnection = true;
            }
            else
            {
                HasActiveConnection = false;
            }

            List<ServerListItemViewModel> serverListItemViewModel_List = new List<ServerListItemViewModel>();
            foreach (ConnectionData connectionData in list)
            {
                ServerListItemViewModel serverListItemViewModel = new ServerListItemViewModel(_NavigationService, _PageDialogService);
                tasks.Add(serverListItemViewModel.LoadInstance(connectionData));
                serverListItemViewModel_List.Add(serverListItemViewModel);

            }
            await Task.WhenAll(tasks).ConfigureAwait(false);
            ServerListItemViewModel_List = serverListItemViewModel_List;

            IsBusy = false;
        }
        #endregion

        #region Fields
        private IList<ServerListItemViewModel> _ServerListItemViewModel_List;
        public IList<ServerListItemViewModel> ServerListItemViewModel_List
        {
            get => _ServerListItemViewModel_List;
            set => SetProperty(ref _ServerListItemViewModel_List, value);
        }

        private ServerListItemViewModel _ActiveConnection;
        public ServerListItemViewModel ActiveConnection
        {
            get => _ActiveConnection;
            set => SetProperty(ref _ActiveConnection, value);
        }

        private bool _HasActiveConnection = false;
        public bool HasActiveConnection
        {
            get => _HasActiveConnection;
            set => SetProperty(ref _HasActiveConnection, value);
        }
        #endregion

        #region Commands
        private ICommand _SelectInstanceCommand;
        public ICommand SelectInstanceCommand
        {
            get => _SelectInstanceCommand;
            set => SetProperty(ref _SelectInstanceCommand, value);
        }
        public async void SelectInstanceCommandExecute(object obj)
        {
            ServerListItemViewModel viewmodel = obj as ServerListItemViewModel;

            NavigationParameters parameters = new NavigationParameters
            {
                { "instance", viewmodel.Instance },
            };

            await _NavigationService.NavigateAsync("ServerPage", parameters).ConfigureAwait(false);
        }

        private ICommand _AddInstancesCommand;
        public ICommand AddInstancesCommand
        {
            get => _AddInstancesCommand;
            set => SetProperty(ref _AddInstancesCommand, value);
        }
        public async void AddInstancesCommandExecute()
        {
            await _NavigationService.NavigateAsync("AddServerProcess_SelectServerPage").ConfigureAwait(false);
        }
        #endregion
    }
}

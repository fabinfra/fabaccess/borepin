﻿using Borepin.ViewModel;
using NaturalSort.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Borepin.Model
{
    public class MachineViewModelListGroup : List<MachineListItemViewModel>
    {
        public string Category { get; set; }
        public void Sort_Machines()
        {
            List<MachineListItemViewModel> ordered = new List<MachineListItemViewModel>(this.OrderBy(x => x.Machine.Name, StringComparison.OrdinalIgnoreCase.WithNaturalSort()));
            Clear();
            AddRange(ordered);
        }
    }
}

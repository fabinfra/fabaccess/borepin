﻿ using FabAccessAPI.Schema;
using Prism.Mvvm;
using System.Threading.Tasks;

namespace Borepin.Model
{
    public class UserVisualize : BindableBase
    {
        #region Private Fields
        public readonly User _User;
        #endregion

        #region Contructors
        public UserVisualize(User user)
        {
            _User = user;
        }
        #endregion

        #region LoadData
        public Task LoadData()
        {
            if(_User == null) 
            {
                return Task.CompletedTask;
            }

            //ID = _User.Id;
            Username = _User.Username;
            //Space = new SpaceVisualize(_User.Space);

            return Task.CompletedTask;
        }
        #endregion

        #region Fields
        private UUID _ID;
        public UUID ID
        {
            get => _ID;
            set => SetProperty(ref _ID, value);
        }

        private string _Username;
        public string Username
        {
            get => _Username;
            set => SetProperty(ref _Username, value);
        }

        private SpaceVisualize _Space;
        public SpaceVisualize Space
        {
            get => _Space;
            set => SetProperty(ref _Space, value);
        }
        #endregion
    }
}

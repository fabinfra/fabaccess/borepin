﻿using FabAccessAPI;
using Prism.Mvvm;

namespace Borepin.Model
{
    public class ConnectionDataVisualize : BindableBase
    {
        #region Private Fields
        public readonly ConnectionData _ConnectionData;
        #endregion

        #region Constructors
        public ConnectionDataVisualize(ConnectionData connectionData)
        {
            _ConnectionData = connectionData;
        }
        #endregion

        #region Methods
        public void LoadData()
        {
            Host = _ConnectionData.Host.Host;
            Port = _ConnectionData.Host.Port;
            Username = _ConnectionData.Username;
        }
        #endregion

        #region Fields
        private string _Host;
        public string Host
        {
            get => _Host;
            set => SetProperty(ref _Host, value);
        }

        private int _Port;
        public int Port
        {
            get => _Port;
            set => SetProperty(ref _Port, value);
        }

        private string _Username;
        public string Username
        {
            get => _Username;
            set => SetProperty(ref _Username, value);
        }
        #endregion
    }
}

﻿using Borepin.Page;
using FabAccessAPI.Schema;
using ImTools;
using NFC.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Borepin.Model
{
    public class DESFireInterfaceDummy : User.ICardDESFireInterface
    {
        public Task Bind(IReadOnlyList<byte> token, IReadOnlyList<byte> auth_key, CancellationToken cancellationToken_ = default)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            
        }

        public Task<IReadOnlyList<byte>> GenCardToken(CancellationToken cancellationToken_ = default)
        {
            List<byte> token = new List<byte>();
            token.AddRange(HexConverter.ConvertFromHexString("11 00 00 00 00 00 00 00 00 00 00 00 00 00 00 11".Replace(" ", "")));
            return Task.FromResult((IReadOnlyList<byte>) token);
        }

        public Task<IReadOnlyList<byte>> GetMetaInfo(CancellationToken cancellationToken_ = default)
        {
            List<byte> data = new List<byte>();
            data.AddRange(Encoding.ASCII.GetBytes("FABACCESS\0DESFIRE\01.0\0").Append((byte)0x00));
            return Task.FromResult((IReadOnlyList<byte>)data);
        }

        public Task<IReadOnlyList<byte>> GetSpaceInfo(CancellationToken cancellationToken_ = default)
        {
            List<byte> data = new List<byte>();
            data.AddRange(Encoding.ASCII.GetBytes("urn:fabaccess:lab:fabaccess_test").Append((byte)0x00));
            return Task.FromResult((IReadOnlyList<byte>)data);
        }

        public Task<IReadOnlyList<IReadOnlyList<byte>>> GetTokenList(CancellationToken cancellationToken_ = default)
        {
            List<byte> token = new List<byte>();
            token.AddRange(HexConverter.ConvertFromHexString("11 00 00 00 00 00 00 00 00 00 00 00 00 00 00 11".Replace(" ", "")));

            IReadOnlyList<IReadOnlyList<byte>> list = new List<List<byte>>
            {
                token
            };
            return Task.FromResult(list);
        }

        public Task Unbind(IReadOnlyList<byte> token, CancellationToken cancellationToken_ = default)
        {
            return Task.CompletedTask;
        }
    }
}

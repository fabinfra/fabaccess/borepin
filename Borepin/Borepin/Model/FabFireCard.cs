﻿using Borepin.Service.ErrorMessage;
using NFC.Cards.NXP_MIFARE_DESFire;
using NFC.Cards.NXP_MIFARE_DESFire.Enums;
using NFC.Helper;
using NFC.Helper.Crypto;
using NFC.Interfaces;
using System;
using System.Text;
using ZXing.Aztec.Internal;

namespace Borepin.Model
{
    public class FabFireCard
    {
        #region Private Fields
        readonly INFCService _NFCService;
        readonly IErrorMessageService _ErrorMessageService;
        #endregion

        #region Constructors
        public FabFireCard(INFCService nfcService, IErrorMessageService errorMessageService)
        {
            _NFCService = nfcService;
            _ErrorMessageService = errorMessageService;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Format Card
        /// </summary>
        /// <param name="readerID"></param>
        /// <param name="cardConfig"></param>
        public void FormatCard(string readerID, CardConfig cardConfig)
        {
            CipherKey PICCKey = new CipherKey(cardConfig.PICCKey, CipherType.TDES, 0x00);

            _NFCService.Connect(readerID);

            NXP_MIFARE_DESFire card = new NXP_MIFARE_DESFire(_NFCService);
            card.SelectApplication(0x000000);
            card.AuthenticateISO_DES(0x00, PICCKey._Key);
            card.Format();

            _NFCService.Disconnect();
        }

        /// <summary>
        /// Create DESFire Card for V1.0
        /// </summary>
        /// <param name="readerID"></param>
        /// <param name="cardConfig"></param>
        /// <returns>Key #1 for authentication </returns>
        public byte[] CreateCard(string readerID, CardConfig cardConfig)
        {
            CipherKey PICCKey = new CipherKey(cardConfig.PICCKey, CipherType.TDES, 0x00);
            CipherKey MasterKey = new CipherKey(cardConfig.APPKey, CipherType.AES, 0x10);
            CipherKey AuthKey = new CipherKey(cardConfig.GenerateRandomKey(), CipherType.AES, 0x10);
            UInt32 AID = 0x464142;

            CipherKey _Default_DESKey = new CipherKey(CipherType.TDES);
            CipherKey _Default_AESKey = new CipherKey(CipherType.AES);

            _NFCService.Connect(readerID);
            NXP_MIFARE_DESFire card = new NXP_MIFARE_DESFire(_NFCService);

            if (cardConfig.DoFormat)
            {
                card.AuthenticateISO_DES(0x00, _Default_DESKey._Key);
            }
            else
            {
                card.AuthenticateISO_DES(0x00, PICCKey._Key);
            }

            byte keySetting1 = card.GenerateKeySetting1(ChangeApplicationKey.MASTERKEY, ChangeMasterKeySettings.WITHMASTERKEY, CreateDeleteFile.ONLYMASTERKEY, FileDirectoryAccess.NOKEY, ChangeMasterKey.CHANGEABLE);
            byte keySetting2 = card.GenerateKeySetting2(CryptoOperationsType.AES, FileIdentifies.NOTUSED, 0x02);
            card.CreateApplication(AID, keySetting1, keySetting2);

            card.SelectApplication(AID);
            card.AuthenticateISO_AES(0x00, _Default_AESKey._Key);

            card.ChangeKey_AES(0x00, MasterKey._Key, MasterKey._KeyVersion);

            card.AuthenticateISO_AES(0x00, MasterKey._Key);
            card.ChangeOtherKey_AES(0x01, AuthKey._Key, _Default_AESKey._Key, AuthKey._KeyVersion);

            UInt16 accessRights = card.GenerateFileAccessRights((byte)FileAccessRights.FREE, 0x00, 0x00, 0x00);

            card.CreateFile_Standard(0x01, FileCommunication.PLAIN, accessRights, (uint)cardConfig.MetaInfo.Length);
            card.CreateFile_Standard(0x02, FileCommunication.PLAIN, accessRights, (uint)cardConfig.SpaceInfo.Length);
            card.CreateFile_Standard(0x03, FileCommunication.PLAIN, accessRights, (uint)47);// TODO (uint)cardConfig.CardToken.Length);

            card.WriteData(0x01, 0x00, cardConfig.MetaInfo);
            card.WriteData(0x02, 0x00, cardConfig.SpaceInfo);
            card.WriteData(0x03, 0x00, _TODOFixDataFileSize(cardConfig.CardToken));

            _NFCService.Disconnect();

            return AuthKey._Key;
        }

        /// <summary>
        /// TODO implement GetFileInfo in DESFire
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private byte[] _TODOFixDataFileSize(byte[] data)
        {
            byte[] array = ByteOperation.GenerateEmptyArray(47);
            data.CopyTo(array, 0);
            return array;
        }
        #endregion
    }
}

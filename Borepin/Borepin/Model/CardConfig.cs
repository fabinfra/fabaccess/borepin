﻿using NFC.Helper;
using System;
using System.Globalization;
using System.Security.Cryptography;

namespace Borepin.Model
{
    public class CardConfig
    {
        #region Constructors
        public CardConfig()
        {
            PICCKey = GenerateEmptyKey();
            APPKey = GenerateEmptyKey();
        }
        #endregion

        #region Fields
        public string UserID;

        public byte[] PICCKey;
        public byte[] APPKey;

        public bool DoFormat;

        public byte[] CardToken;
        public byte[] MetaInfo;
        public byte[] SpaceInfo;
        #endregion

        #region Mehtods
        public string ConvertToString(byte[] array)
        {
            string data = HexConverter.ConvertToHexString(array);
            data = data.ToUpper(CultureInfo.InvariantCulture);

            for(int i = 2; i < data.Length; i += 3) 
            {
                data = data.Insert(i, " ");
            }

            return data;
        }

        public byte[] ConvertFromString(string data)
        {
            data = data.Trim();
            data = data.Replace(" ", "");

            byte[] array = HexConverter.ConvertFromHexString(data);
            return array;
        }

        public byte[] GenerateRandomKey()
        {
            byte[] key = ByteOperation.GenerateEmptyArray(16);
            RNGCryptoServiceProvider cryptoProvider = new RNGCryptoServiceProvider();
            cryptoProvider.GetBytes(key);
            return key;
        }

        public byte[] GenerateEmptyKey()
        {
            return ByteOperation.GenerateEmptyArray(16);
        }
        #endregion
    }
}

﻿namespace Borepin.Model
{
    public class KeyScan
    {
        public KeyScan(CardConfig cardConfig, KeyTypes keyType) 
        {
            CardConfig = cardConfig;
            KeyType = keyType;
        }

        public CardConfig CardConfig;
        public KeyTypes KeyType;
    }
}

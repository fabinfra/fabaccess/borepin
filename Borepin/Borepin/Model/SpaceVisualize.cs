﻿using FabAccessAPI.Schema;
using Prism.Mvvm;

namespace Borepin.Model
{
    public class SpaceVisualize : BindableBase
    {
        #region Private Fields
        public readonly Space _Space;
        #endregion

        #region Constructors
        public SpaceVisualize(Space space)
        {
            _Space = space;
        }
        #endregion

        #region LoadData
        public void LoadData()
        {
            //ID = _Space.Id;
            Name = _Space.Name;
            Info = _Space.Info;
        }
        #endregion

        #region Fields
        private UUID _ID;
        public UUID ID
        {
            get => _ID;
            set => SetProperty(ref _ID, value);
        }

        private string _Name;
        public string Name
        {
            get => _Name;
            set => SetProperty(ref _Name, value);
        }

        private string _Info;
        public string Info
        {
            get => _Info;
            set => SetProperty(ref _Info, value);
        }
        #endregion
    }
}

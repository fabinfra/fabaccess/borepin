﻿using FabAccessAPI.Schema;
using Prism.Mvvm;
using System.Threading.Tasks;
using static FabAccessAPI.Schema.Machine;

namespace Borepin.Model
{
    public class MachineVisualize : BindableBase
    {
        #region Private Fields
        public readonly Machine _Machine;
        #endregion

        #region Constructors
        public MachineVisualize(Machine machine)
        {
            _Machine = machine;
        }
        #endregion

        #region Methods
        public async Task LoadData()
        {
            if(_Machine== null) 
            {
                return;
            }

            //ID = _Machine.Id;
            //Space = new SpaceVisualize(_Machine.Space);
            Name = _Machine.Name;
            Description = _Machine.Description;
            Wiki = _Machine.Wiki;
            State = _Machine.State;
            Category = _Machine.Category;

            if(_Machine.Manager.Just != null)
            {
                Manager = new UserVisualize(_Machine.Manager.Just);
                await Manager.LoadData().ConfigureAwait(false);
            }
            else
            {
                Manager = null;
            }

            if(!((ManageInterface_Proxy)_Machine.Manage).IsNull)
            {
                MachineInfoExtended machineInfoExtended = await _Machine.Manage.GetMachineInfoExtended().ConfigureAwait(false);
                if (machineInfoExtended.CurrentUser.Just == null)
                {
                    CurrentUser = null;
                }
                else
                {
                    CurrentUser = new UserVisualize(machineInfoExtended.CurrentUser.Just);
                    await CurrentUser.LoadData().ConfigureAwait(false);
                }

                if (machineInfoExtended.LastUser.Just == null)
                {
                    LastUser = null;
                }
                else
                {
                    LastUser = new UserVisualize(machineInfoExtended.LastUser.Just);
                    await LastUser.LoadData().ConfigureAwait(false);
                }
            }
            else
            {
                CurrentUser = null;
                LastUser = null;
            }

            CanUse = !((UseInterface_Proxy)_Machine.Use).IsNull;
            CanInUse = !((InUseInterface_Proxy)_Machine.Inuse).IsNull;
            CanTakeOver = !((TakeoverInterface_Proxy)_Machine.Takeover).IsNull;
            CanCheck = !((CheckInterface_Proxy)_Machine.Check).IsNull;
            CanManage = !((ManageInterface_Proxy)_Machine.Manage).IsNull;
            CanAdmin = !((AdminInterface_Proxy)_Machine.Admin).IsNull;
            CanNotUseByPermission = State == MachineState.free && !CanUse;
            IsLock = !((ProdInterface_Proxy)_Machine.Prodable).IsNull;
        }
        #endregion

        #region Fields
        private UUID _ID;
        public UUID ID
        {
            get => _ID;
            set => SetProperty(ref _ID, value);
        }

        private SpaceVisualize _Space;
        public SpaceVisualize Space
        {
            get => _Space;
            set => SetProperty(ref _Space, value);
        }

        private string _Name;
        public string Name
        {
            get => _Name;
            set => SetProperty(ref _Name, value);
        }

        private string _Category;
        public string Category
        {
            get => _Category;
            set => SetProperty(ref _Category, value);
        }

        private string _Description;
        public string Description
        {
            get => _Description;
            set => SetProperty(ref _Description, value);
        }

        private string _Wiki;
        public string Wiki
        {
            get => _Wiki;
            set => SetProperty(ref _Wiki, value);
        }

        private MachineState _State;
        public MachineState State
        {
            get => _State;
            set => SetProperty(ref _State, value);
        }

        private UserVisualize _Manager;
        public UserVisualize Manager
        {
            get => _Manager;
            set => SetProperty(ref _Manager, value);
        }

        private UserVisualize _CurrentUser;
        public UserVisualize CurrentUser
        {
            get => _CurrentUser;
            set => SetProperty(ref _CurrentUser, value);
        }

        private UserVisualize _LastUser;
        public UserVisualize LastUser
        {
            get => _LastUser;
            set => SetProperty(ref _LastUser, value);
        }

        private bool _CanUse;
        public bool CanUse
        {
            get => _CanUse;
            set => SetProperty(ref _CanUse, value);
        }

        private bool _CanInUse;
        public bool CanInUse
        {
            get => _CanInUse;
            set => SetProperty(ref _CanInUse, value);
        }

        private bool _CanTransfer;
        public bool CanTakeOver
        {
            get => _CanTransfer;
            set => SetProperty(ref _CanTransfer, value);
        }

        private bool _CanCheck;
        public bool CanCheck
        {
            get => _CanCheck;
            set => SetProperty(ref _CanCheck, value);
        }

        private bool _CanManage;
        public bool CanManage
        {
            get => _CanManage;
            set => SetProperty(ref _CanManage, value);
        }

        private bool _CanAdmin;
        public bool CanAdmin
        {
            get => _CanAdmin;
            set => SetProperty(ref _CanAdmin, value);
        }

        private bool _CanNotUseByPermission;
        public bool CanNotUseByPermission
        {
            get => _CanNotUseByPermission;
            set => SetProperty(ref _CanNotUseByPermission, value);
        }

        private bool _IsLock;
        public bool IsLock
        {
            get => _IsLock;
            set => SetProperty(ref _IsLock, value);
        }
        #endregion
    }
}

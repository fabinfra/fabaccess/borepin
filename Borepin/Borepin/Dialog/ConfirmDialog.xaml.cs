﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.Dialog
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmDialog : ContentView
    {
        public ConfirmDialog()
        {
            InitializeComponent();
        }
    }
}
﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.Page.SetUpProcess
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage : ContentPage
    {
        public WelcomePage()
        {
            InitializeComponent();
        }
    }
}
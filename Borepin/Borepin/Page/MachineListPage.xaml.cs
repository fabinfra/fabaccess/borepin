﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MachineListPage : ContentPage
    {
        public MachineListPage()
        {
            InitializeComponent();
        }
    }
}
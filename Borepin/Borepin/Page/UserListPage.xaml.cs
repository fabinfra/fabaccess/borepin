﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Borepin.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserListPage : ContentPage
    {
        public UserListPage()
        {
            InitializeComponent();
        }
    }
}
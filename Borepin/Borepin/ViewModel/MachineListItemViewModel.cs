﻿using FabAccessAPI.Schema;
using System.Threading.Tasks;
using System.Windows.Input;
using Borepin.Base;
using Prism.Navigation;
using Prism.Services;
using Prism.Commands;
using Xamarin.Forms;
using Borepin.Model;

namespace Borepin.ViewModel
{
    public class MachineListItemViewModel : PageModelBase
    {
        #region Constructors
        public MachineListItemViewModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService, pageDialogService)
        {
            SelectInstanceCommand = new DelegateCommand(SelectInstanceCommandExecute);
        }
        #endregion

        #region LoadData
        public override async Task LoadInstance(object instance)
        {
            if(instance is Machine)
            {
                Machine = new MachineVisualize(instance as Machine);
                await Machine.LoadData().ConfigureAwait(false);
            }
        }
        #endregion

        #region Commands
        private ICommand _SelectInstanceCommand;
        public ICommand SelectInstanceCommand
        {
            get => _SelectInstanceCommand;
            set => SetProperty(ref _SelectInstanceCommand, value);
        }
        public void SelectInstanceCommandExecute()
        {
            NavigationParameters parameters = new NavigationParameters
            {
                { "instance", Machine._Machine.Id },
            };

            Device.BeginInvokeOnMainThread(async () =>
            {
                INavigationResult result = await _NavigationService.NavigateAsync("MachinePage", parameters).ConfigureAwait(false);
                if (result.Exception != null)
                {
                    Log.Fatal(result.Exception, "Navigating failed");
                }
            });
        }

        #endregion

        #region Fields
        private MachineVisualize _Machine;
        public MachineVisualize Machine
        {
            get => _Machine;
            set => SetProperty(ref _Machine, value);
        }

        private bool _IsInUseByMe = false;
        public bool IsInUseByMe
        {
            get => _IsInUseByMe;
            set => SetProperty(ref _IsInUseByMe, value);
        }
        #endregion
    }
}

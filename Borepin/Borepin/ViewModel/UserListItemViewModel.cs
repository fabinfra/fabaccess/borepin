﻿using Borepin.Base;
using Borepin.Model;
using FabAccessAPI;
using FabAccessAPI.Schema;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Borepin.ViewModel
{
    public class UserListItemViewModel : PageModelBase
    {
        #region Constructors
        public UserListItemViewModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService, pageDialogService)
        {
            SelectInstanceCommand = new DelegateCommand(SelectInstanceCommandExecute);
        }
        #endregion

        #region LoadData
        public override Task LoadInstance(object instance)
        {
            if(instance is User)
            {
                User = new UserVisualize(instance as User);
                User.LoadData();
            }
            return Task.CompletedTask;
        }
        #endregion

        #region Fields
        public readonly User Instance;

        private UserVisualize _User;
        public UserVisualize User
        {
            get => _User;
            set => SetProperty(ref _User, value);
        }
        #endregion

        #region Commands
        private ICommand _SelectInstanceCommand;
        public ICommand SelectInstanceCommand
        {
            get => _SelectInstanceCommand;
            set => SetProperty(ref _SelectInstanceCommand, value);
        }
        public void SelectInstanceCommandExecute()
        {
            NavigationParameters parameters = new NavigationParameters
            {
                { "instance", User.Username },
            };

            Device.BeginInvokeOnMainThread(async () =>
            {
                INavigationResult result = await _NavigationService.NavigateAsync("UserPage", parameters).ConfigureAwait(false);
                if (result.Exception != null)
                {
                    Log.Fatal(result.Exception, "Navigating failed");
                }
            });
        }
        #endregion
    }
}

﻿using Borepin.Base;
using Borepin.Model;
using FabAccessAPI;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Borepin.ViewModel
{
    public class ServerListItemViewModel : PageModelBase
    {
        #region Constructors
        public ServerListItemViewModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService, pageDialogService)
        {
            SelectInstanceCommand = new DelegateCommand(SelectInstanceCommandExecute);
        }
        #endregion

        #region LoadData
        public override Task LoadInstance(object instance)
        {
            if(instance is ConnectionData)
            {
                ConnectionData = new ConnectionDataVisualize(instance as ConnectionData);
                ConnectionData.LoadData();
            }
            return Task.CompletedTask;
        }
        #endregion

        #region Fields
        public readonly ConnectionData Instance;

        private ConnectionDataVisualize _ConnectionData;
        public ConnectionDataVisualize ConnectionData
        {
            get => _ConnectionData;
            set => SetProperty(ref _ConnectionData, value);
        }
        #endregion

        #region Commands
        private ICommand _SelectInstanceCommand;
        public ICommand SelectInstanceCommand
        {
            get => _SelectInstanceCommand;
            set => SetProperty(ref _SelectInstanceCommand, value);
        }
        public void SelectInstanceCommandExecute()
        {
            NavigationParameters parameters = new NavigationParameters
            {
                { "instance", ConnectionData._ConnectionData },
            };

            Device.BeginInvokeOnMainThread(async () =>
            {
                INavigationResult result = await _NavigationService.NavigateAsync("ServerPage", parameters).ConfigureAwait(false);
                if (result.Exception != null)
                {
                    Log.Fatal(result.Exception, "Navigating failed");
                }
            });
        }
        #endregion
    }
}

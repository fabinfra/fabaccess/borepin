﻿using Borepin.Service;
using FabAccessAPI;
using NLog;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Threading.Tasks;

namespace Borepin.Base
{
    /// <summary>
    /// Base for all BFFH Based PageModels
    /// </summary>
    public abstract class ConnectionModelBase : PageModelBase
    {
        #region Private Fields
        protected readonly IAPI _API;
        protected readonly IAPIService _APIService;
        #endregion

        #region Constructors
        protected ConnectionModelBase(INavigationService navigationService, IPageDialogService pageDialogService, IAPIService apiService) : base(navigationService, pageDialogService)
        {
            _APIService = apiService;

            _API = apiService.GetAPI();
            _API.ConnectionStatusChanged += OnConnectionStatusChanged;

            IsConnected = _API.IsConnected;
        }
        #endregion

        #region Methods
        public async void OnConnectionStatusChanged(object sender, ConnectionStatusChanged args)
        {
            switch(args)
            {
                case ConnectionStatusChanged.Connected:
                    IsConnected = true;
                    IsConnecting = false;
                    try
                    {
                        await LoadAPIData().ConfigureAwait(false);
                    }
                    catch(Exception exception)
                    {
                        Log.Warn("Load API Data failed", exception);
                    }
                    break;
                case ConnectionStatusChanged.ConnectionLoss:
                    IsConnected = false;
                    IsConnecting = true;
                    break;
                case ConnectionStatusChanged.Disconnected:
                    IsConnected = false;
                    IsConnecting = false;
                    break;
            }
        }

        public virtual Task LoadAPIData()
        {
            return Task.CompletedTask;
        }
        #endregion

        #region Fields
        /// <summary>
        /// PageModel is Connected
        /// </summary>
        private bool _IsConnected = false;
        public bool IsConnected
        {
            get => _IsConnected;
            set => SetProperty(ref _IsConnected, value);
        }

        /// <summary>
        /// PageModel is Connecting
        /// </summary>
        private bool _IsConnecting = false;
        public bool IsConnecting
        {
            get => _IsConnecting;
            set => SetProperty(ref _IsConnecting, value);
        }
        #endregion

        #region INavigationAware
        public override async Task OnNavigatedToVirtual(INavigationParameters parameters)
        {
            await base.OnNavigatedToVirtual(parameters).ConfigureAwait(false);

            if(_API.IsConnected)
            {
                try
                {
                    await LoadAPIData().ConfigureAwait(false);
                }
                catch(Exception exception)
                {
                    Log.Warn("Load API Data failed", exception);
                }
            }
        }
        #endregion
    }
}
﻿using System;

namespace Borepin.Base.Exceptions
{
    public class InstanceIncorrectException : Exception
    {
        public InstanceIncorrectException()
        {

        }

        public InstanceIncorrectException(string message) : base(message)
        {

        }

        public InstanceIncorrectException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}

﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Windows.Input;

namespace Borepin.DialogModel
{
    /// <summary>
    /// Confirm Dialog
    /// </summary>
    public class ConfirmDialogModel : BindableBase, IDialogAware
    {
        #region Private Fields
        private object _Instance;
        #endregion

        #region Constructors
        public ConfirmDialogModel()
        {
            ConfirmCommand = new DelegateCommand(ConfirmCommandExecute);
            AbortCommand = new DelegateCommand(AbortCommandExecute);
        }
        #endregion

        #region Fields
        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        private string _Title;
        public string Title
        {
            get => _Title;
            set => SetProperty(ref _Title, value);
        }

        private string _Message;
        public string Message
        {
            get => _Message;
            set => SetProperty(ref _Message, value);
        }
        #endregion

        #region Commands
        private ICommand _ConfirmCommand;

        public ICommand ConfirmCommand
        {
            get => _ConfirmCommand;
            set => SetProperty(ref _ConfirmCommand, value);
        }
        public void ConfirmCommandExecute()
        {
            IDialogParameters parameters = new DialogParameters()
            {
                { "result", "confirm" },
                { "instance", _Instance },
            };
            RequestClose(parameters);
        }

        private ICommand _AbortCommand;
        public ICommand AbortCommand
        {
            get => _AbortCommand;
            set => SetProperty(ref _AbortCommand, value);
        }
        public void AbortCommandExecute()
        {
            IDialogParameters parameters = new DialogParameters()
            {
                { "result", "abort" },
                { "instance", _Instance },
            };
            RequestClose(parameters);
        }
        #endregion

        #region IDialogAware
        public void OnDialogClosed()
        {
            
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            Title = parameters.GetValue<string>("title");
            Message = parameters.GetValue<string>("message");
            _Instance = parameters.GetValue<object>("instance");
        }
        #endregion
    }
}

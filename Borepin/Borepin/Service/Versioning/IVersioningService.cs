﻿namespace Borepin.Service.Versioning
{
    public interface IVersioningService
    {
        string CurrentBuild { get; }
        string CurrentVersion { get; }
    }
}

﻿namespace Borepin.Service.Browser
{
    public enum BrowserLaunchMode
    {
        SystemPreferred = 0,
        External = 1,
    }
}

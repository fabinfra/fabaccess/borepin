﻿using System;
using System.Drawing;

namespace Borepin.Service.Browser
{
    public class BrowserLaunchOptions
    {
        public BrowserLaunchFlags Flags;
        public BrowserLaunchMode LaunchMode;
        public Nullable<Color> PreferredControlColor;
        public Nullable<Color> PreferredToolbarColor;
        public BrowserTitleMode TitleMode;
    }
}

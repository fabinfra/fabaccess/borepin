﻿namespace Borepin.Service.Browser
{
    public enum BrowserTitleMode
    {
        Default = 0,
        Show = 1,
        Hide = 2,
    }
}

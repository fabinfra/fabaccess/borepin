﻿using System;
using System.Threading.Tasks;

namespace Borepin.Service.Browser
{
    public interface IBrowserService
    {
        Task OpenAsync(string uri);
        Task OpenAsync(string uri, BrowserLaunchMode browserLaunchMode);
        Task OpenAsync(string uri, BrowserLaunchOptions browserLaunchOptions);
        Task OpenAsync(Uri uri);
        Task OpenAsync(Uri uri, BrowserLaunchMode browserLaunchMode);
        Task OpenAsync(Uri uri, BrowserLaunchOptions browserLaunchOptions);


    }
}

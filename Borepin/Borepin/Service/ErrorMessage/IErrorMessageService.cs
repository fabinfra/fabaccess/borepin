﻿using FabAccessAPI.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Borepin.Service.ErrorMessage
{
    public interface IErrorMessageService
    {
        void DisplayConnectFailedMessage(Exception exception);
        void DisplayConnectionMessage(ConnectionException exception);
        void DisplayAuthenticationMessage(AuthenticationException exception);
    }
}

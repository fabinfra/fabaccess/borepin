﻿using FabAccessAPI;

namespace Borepin.Service
{
    public interface IAPIService
    {
        IAPI GetAPI();
    }
}

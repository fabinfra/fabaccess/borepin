﻿using FabAccessAPI;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Borepin.Service.Storage
{
    public interface ILoginStorageService
    {
        Task<IList<ConnectionData>> GetList();
        Task Add(ConnectionData connectionData);
        Task Remove(ConnectionData connectionData);
        Task UpdateTimestamp(ConnectionData connectionData);

        Task<ConnectionData> GetDefault();
        Task SetDefault(ConnectionData connectionData);
        Task RemoveDefault();
    }
}

﻿namespace Borepin.Service.Storage
{
    /// <summary>
    /// Interface to abstract Xamarin.Essentials.Preference
    /// </summary>
    public interface IPreferenceStorageService
    {
        void Clear();

        bool ContainsKey(string key);

        string Get(string key, string defaultValue);

        void Remove(string key);

        void Set(string key, string value);
    }
}

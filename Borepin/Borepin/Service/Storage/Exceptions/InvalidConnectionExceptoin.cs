﻿using FabAccessAPI;
using System;

namespace Borepin.Service.Storage.Exceptions
{
    [Serializable]
    internal class InvalidConnectionExceptoin : Exception
    {
        public  readonly ConnectionData ConnectionData;

        public InvalidConnectionExceptoin(ConnectionData connectionData)
        {
            ConnectionData = connectionData;
        }

        public InvalidConnectionExceptoin(ConnectionData connectionData, string message) : base(message)
        {
            ConnectionData = connectionData;
        }

        public InvalidConnectionExceptoin(ConnectionData connectionData, string message, Exception innerException) : base(message, innerException)
        {
            ConnectionData = connectionData;
        }
    }
}
﻿using FabAccessAPI;
using System;

namespace Borepin.Service.Storage.Exceptions
{
    public class DuplicateConnectionException : Exception
    {
        public readonly ConnectionData ConnectionData;
        public DuplicateConnectionException(ConnectionData connectionData)
        {
            ConnectionData = connectionData;
        }

        public DuplicateConnectionException(ConnectionData connectionData, string message) : base(message)
        {
            ConnectionData = connectionData;
        }

        public DuplicateConnectionException(ConnectionData connectionData, string message, Exception inner) : base(message, inner)
        {
            ConnectionData = connectionData;
        }
    }
}

﻿using FabAccessAPI;
using System;

namespace Borepin.Service.Storage.Exceptions
{
    public class MissingConnectionException : Exception
    {
        public readonly ConnectionData ConnectionData;
        public MissingConnectionException(ConnectionData connectionData)
        {
            ConnectionData = connectionData;
        }

        public MissingConnectionException(ConnectionData connectionData, string message) : base(message)
        {
            ConnectionData = connectionData;
        }

        public MissingConnectionException(ConnectionData connectionData, string message, Exception inner) : base(message, inner)
        {
            ConnectionData = connectionData;
        }
    }
}

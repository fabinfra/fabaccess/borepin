﻿using System.Threading.Tasks;

namespace Borepin.Service.Storage
{
    /// <summary>
    /// Interface to abstract Xamarin.Essentials.SecureStorage
    /// </summary>
    public interface ISecretStorageService
    {
        Task<string> GetAsync(string key);

        bool Remove(string key);

        void RemoveAll();

        Task SetAsync(string key, string value);
    }
}

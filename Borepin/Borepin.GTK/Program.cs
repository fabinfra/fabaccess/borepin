﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.GTK;

namespace Borepin.GTK
{
    class MainClass
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Gtk.Application.Init();
            Forms.Init();

            FormsWindow window = new FormsWindow();
            window.LoadApplication(new App(new PlatformInitializer()));
            window.SetApplicationTitle("FabAccess");
            window.Show();

            Gtk.Application.Run();
        }
    }
}
﻿using Borepin.iOS.Services;
using Borepin.Service;
using Borepin.Service.Browser;
using Borepin.Service.Storage;
using Borepin.Service.Versioning;
using Prism;
using Prism.Ioc;

namespace Borepin.iOS
{
    public class PlatformInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IPreferenceStorageService, PreferenceStorageService>();
            containerRegistry.Register<ISecretStorageService, SecretStorageService>();
            containerRegistry.Register<IVersioningService, VersioningService>();
            containerRegistry.Register<IBrowserService, BrowserService>();
            // TODO containerRegistry.Register<INFCService, NFCService>();

            containerRegistry.RegisterSingleton<IAPIService, APIService>();
        }
    }
}
﻿using Borepin.Service.Browser;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Borepin.iOS.Services
{
    public class BrowserService : IBrowserService
    {
        private Xamarin.Essentials.BrowserLaunchOptions _ConvertBrowserLaunchOptions(Service.Browser.BrowserLaunchOptions browserLaunchOptions)
        {
            return new Xamarin.Essentials.BrowserLaunchOptions()
            {
                Flags = (Xamarin.Essentials.BrowserLaunchFlags)browserLaunchOptions.Flags,
                LaunchMode = (Xamarin.Essentials.BrowserLaunchMode)browserLaunchOptions.LaunchMode,
                PreferredControlColor = browserLaunchOptions.PreferredControlColor,
                PreferredToolbarColor = browserLaunchOptions.PreferredToolbarColor,
                TitleMode = (Xamarin.Essentials.BrowserTitleMode)browserLaunchOptions.TitleMode
            };
        }
        public async Task OpenAsync(string uri)
        {
            await Browser.OpenAsync(uri).ConfigureAwait(false);
        }

        public async Task OpenAsync(string uri, Service.Browser.BrowserLaunchMode browserLaunchMode)
        {
            await Browser.OpenAsync(uri, (Xamarin.Essentials.BrowserLaunchMode)browserLaunchMode).ConfigureAwait(false);
        }

        public async Task OpenAsync(string uri, Service.Browser.BrowserLaunchOptions browserLaunchOptions)
        {
            await Browser.OpenAsync(uri, _ConvertBrowserLaunchOptions(browserLaunchOptions));
        }

        public async Task OpenAsync(Uri uri)
        {
            await Browser.OpenAsync(uri).ConfigureAwait(false);
        }

        public async Task OpenAsync(Uri uri, Service.Browser.BrowserLaunchMode browserLaunchMode)
        {
            await Browser.OpenAsync(uri, (Xamarin.Essentials.BrowserLaunchMode)browserLaunchMode).ConfigureAwait(false);
        }

        public async Task OpenAsync(Uri uri, Service.Browser.BrowserLaunchOptions browserLaunchOptions)
        {
            await Browser.OpenAsync(uri, _ConvertBrowserLaunchOptions(browserLaunchOptions));
        }
    }
}
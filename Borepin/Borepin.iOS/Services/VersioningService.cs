﻿using Borepin.Service.Versioning;
using Xamarin.Essentials;

namespace Borepin.iOS.Services
{
    public class VersioningService : IVersioningService
    {
        #region Constructors
        public VersioningService()
        {
            VersionTracking.Track();
        }
        #endregion
        public string CurrentBuild
        {
            get
            {
                return VersionTracking.CurrentBuild;
            }
        }

        public string CurrentVersion
        {
            get
            {
                return VersionTracking.CurrentVersion;
            }
        }
    }
}
﻿using System;
using Android.App;
using Android.Runtime;

namespace Borepin.Droid
{
    [Application(Label = "FabAccess", Icon = "@mipmap/ic_launcher")]
    public class MainApplication : Application
    {
        public MainApplication(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {

        }
    }
}
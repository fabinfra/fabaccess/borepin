# Borepin

Borepin is the client application for using FabAccess. Borepin is written in C# with [Xamarin](https://dotnet.microsoft.com/apps/xamarin) for UI. We use [Prism Libary](https://prismlibrary.com/docs/xamarin-forms/Getting-Started.html) for MVVM and navigation managment.

# Download

Please see [https://fab-access.org/download](https://fab-access.org/download) for all download and build options!

# Server
Yoh will find proper documentation for setting up your own BFFH FabAccess Server at [https://fab-access.org/install](https://fab-access.org/install)